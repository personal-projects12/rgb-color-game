let numberOfSquares = 6;
let colors = [];
let pickedColor;
let squares = document.querySelectorAll('.square');
let colorDisplay = document.querySelector('#color-display');
let selectedClass = document.querySelector('.selected');
let messageDisplay = document.querySelector('#message');
let header = document.querySelector('header');
let modeButtons  = document.querySelectorAll('.mode');
let resetBtn = document.querySelector('#reset');

init();

function init () {
  setupModeButtons();
  setupSquares();
  reset();
}

function setupModeButtons () {
  for (let i = 0; i < modeButtons.length; i++) {
    modeButtons[i].addEventListener('click', function (){
      modeButtons[0].classList.remove('selected');
      modeButtons[1].classList.remove('selected');
      this.classList.add('selected');
      this.textContent === "Easy" ? numberOfSquares = 3 : numberOfSquares = 6;
      reset();
    });
  }
}

function setupSquares () {
  for (let i =0; i < squares.length; i++) {
    // add click listeners
    squares[i].addEventListener('click', function () {
      // grab color of picked square
      let clickedColor = this.style.backgroundColor;
      // compare color to pickedColor
      if (clickedColor === pickedColor) {
        changeColors(clickedColor);
        header.style.backgroundColor = clickedColor;
        messageDisplay.textContent = "Correct!";
        resetBtn.textContent = "Play again?"
      } else {
        this.style.backgroundColor = "#fcfcfc";
        this.style.boxShadow = "none";
        messageDisplay.textContent = "Try Again!";
      }
    })
  }
}

function reset () {
  // generate all new colors
  colors = generateRandomColors(numberOfSquares);
  // pick a random color from array
  pickedColor = pickColor();
  // change colorDisplay to match picked color
  colorDisplay.textContent = pickedColor;
  resetBtn.textContent= "Generate New Colors";
  messageDisplay.textContent = "";
  // change colors of squares
  for (var i =0; i < squares.length; i++) {
    if (colors[i]) {
      squares[i].style.display = "block";
      squares[i].style.backgroundColor = colors[i];
    } else {
      squares[i].style.display = "none";
    }
  }
  header.style.backgroundColor = "steelblue";
}

function changeColors (color) {
  // loop through all the squares
  for (var i =0; i < squares.length; i++) {
    //change each color to match color
    squares[i].style.backgroundColor = color;
  }
}

function pickColor () {
  //Pick a random number based on the length of the colors array
  let random = Math.floor(Math.random() * colors.length);
  return colors[random];
}

function generateRandomColors (numberOfColors) {
  // take in a parameter of how many colors you want to generate
  // make an array
  let arr = [];
  // repeat numberOfColors
  for (let i = 0; i < numberOfColors; i++) {
    // get random color and push into an array
    arr.push(randomColor());
  }
  // return that array
  return arr;
}

function randomColor () {
  // pick a red from 0 - 255
  let r = Math.floor(Math.random() * 256);
  // pick a green from 0 - 255
  let g = Math.floor(Math.random() * 256);
  // pick a blue from 0 -255
  let b = Math.floor(Math.random() * 256);

  return `rgb(${r}, ${g}, ${b})`;
}

resetBtn.addEventListener('click', function () {
  reset();
});